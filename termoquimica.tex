\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
%\usepackage[spanish]{babel}
\usepackage[T1]{fontenc}
\usepackage[left=2.6 cm, right=2.8 cm, top=2.7cm, bottom=3 cm]{geometry}
\usepackage{amsmath}
\usepackage{enumitem} % para continuar la numeracion entre enumerate
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx} % requiere paquete texlive-full
\usepackage{pdfpages}
\usepackage{hyperref}
\usepackage{pgfplots} % llama al paquete tikz
\usepackage[spanish,es-noshorthands]{babel} %elimina conflicto con tikz con las
% comillas latinas <<>>, usar \begin{quoting} como sustituto
\usepackage{tikz-3dplot}

% Apuntes y resoluciones de ejercicios propios y enunciados de ejercicios 
% extraídos la mayoría del libro Física y química 1º Bachillerato 1ª edición de 
% la editorial Oxford de los autores Mario Ballestero Jadraque y Jorge Barrio 
% Gómez de Agüero (supongo que estaré infringiendo la licencia del libro, 
% pueden ponerme los grilletes...)
% luis.munoz.edu at juntadeandalucia.es

%\usepackage{enumitem}
\newcommand{\abs}[1]{\lvert#1\rvert}

%\pagestyle{headings}


\begin{document}
\textbf{\begin{Large}\begin{center}TERMOQUÍMICA\end{center}\end{Large}}
versión 1.1 --- \today

\begin{footnotesize}
\href{https://bitbucket.org/llantones/apuntesfq/src/master/}{
https://bitbucket.org/llantones/apuntesfq/src/master/}

luis.munoz.edu at juntadeandalucia.es
\end{footnotesize}

\vspace{1cm}

La termoquímica estudia los intercambios de energía que se producen en las 
reacciones químicas.


\section {Energía interna}

La temperatura es una magnitud proporcional a la energía cinética media de las 
partículas que forman un cuerpo. Es una magnitud intensiva puesto que no 
depende del tamaño de la muestra.

Cuando un objeto cae sabemos que su energía potencial se 
transforma en energía cinética, 
pero cuando llega al suelo, ¿qué le ocurre a la energía cinética? Sabemos 
que se incrementa la temperatura del objeto, del suelo y del aire, por tanto, 
para que se conserve la energía,  
asignamos a los cuerpos una energía interna ($U$) de tal manera que su energía 
total es: 
$$E_{\mathrm{total}}=E_\mathrm{c}+E_\mathrm{p}+U$$ 
donde la energía interna es 
la suma de las energías cinética y potencial para todas las 
partículas microscópicas que forman el cuerpo:
$$U=\sum_{\mathrm{part\acute{\dotlessi}culas}}(E_\mathrm{c}+E_\mathrm{p})_{
\mathrm{microsc\acute{o } pica} } $$

\noindent y la energía potencial microscópica es debida a la interacción 
entre las partículas (es mayor en estado sólido que en gaseoso). Para 
un gas 
ideal la energía interna solo depende de la temperatura (en 
un gas ideal las partículas no interaccionan entre sí por lo que su energía 
potencial es cero). La energía interna es una magnitud extensiva, pues depende 
del tamaño de la 
muestra, y se intercambia cuando los cuerpos se encuentran a diferente 
temperatura.
Para comprender la diferencia entre temperatura y energía interna consideremos 
dos cuerpos de igual material pero de diferente tamaño puestos en 
contacto según la figura~\ref{energia_interna} en las siguientes situaciones:


\begin{figure}[ht]
\centering
\begin{tikzpicture}
\pgfplotsset{width=7cm,compat=1.16}
\draw[] (0,0) rectangle (3,1.5);
\draw[] (3.1,0) rectangle (5,1.5);
\node at (1.5,-0.4){$T_1, v_1, U_1$};
\node at (4.1,-0.4){$T_2, v_2, U_2$};

\end{tikzpicture}
\caption{Temperatura y energía interna}
\label{energia_interna}
\end{figure}

\begin{enumerate}[resume]
 \item $T_1=T_2$ (equilibrio térmico), $v_1=v_2$ (igual velocidad molecular) y 
$E_\mathrm{interna\ 1} > 
E_\mathrm{interna\ 2}$ puesto que el cuerpo 1 es mayor que el 2 y por tanto 
tiene más partículas.
 \item $T_1 < T_2$, $v_1 < v_2$ y se transfiere energía interna en los choques 
entre las partículas del cuerpo 2 al 1 hasta que se igualan las velocidades 
moleculares y las temperaturas. Esta transferencia de energía se llama calor.
\end{enumerate}


El calor se transfiere por conducción (como en el ejemplo anterior), convección 
y radiación.

Solo existen 
dos maneras de intercambiar energía: calor y trabajo. El calor es el 
intercambio de energía interna debido a la diferencia de temperatura. El 
trabajo es el intercambio de energía realizado mediante fuerzas.

El equivalente mecánico del calor lo calculó James Prescott Joule: 
$$1\ \mathrm{cal}=4,184\  \mathrm{J}$$ 
\subsubsection*{Ejercicios}
\begin{enumerate}
\item Dibuja el dispositivo experimental que usó James Joule para calcular 
el equivalente mecánico y explica su funcionamiento.
\end{enumerate}


 \section {Primera ley de la termodinámica}
La energía interna de un sistema se modifica debido al intercambio de 
calor o a la realización de trabajo según la expresión: 
$$\Delta U=Q+W$$

La energía interna es una función de estado puesto que su valor solo depende de 
los estados inicial y final sin embargo el calor y el trabajo no son funciones 
de estado puesto que sus valores dependen del camino seguido para ir del estado 
inicial al final.

\begin{itemize}
 \item $\mathrm{Q}>0$ y $\mathrm{W}>0$ significa que el sistema aumenta su 
$\mathrm{U}$.
\item $\mathrm{Q}<0$ y $\mathrm{W}<0$ significa que el sistema disminuye su 
$\mathrm{U}$.

\end{itemize}



\subsection{Trabajo \textit{P-V}}
El trabajo que realiza una fuerza constante sobre un cuerpo es igual al 
producto escalar entre la fuerza aplicada y el vector desplazamiento:
$$W=\vec F \cdot \vec{\Delta x} $$
y una vez realizado el producto escalar tenemos:
$$W=\abs{\vec F} \cdot \abs{\vec{\Delta x}}\cdot \mathrm{cos} \, \phi $$

En termoquímica es habitual que el trabajo sea debido a un cambio de 
volumen. Consideremos un cilindro y un émbolo o pistón en su interior 
(figuras~\ref{cilindro1} y \ref{cilindro2}). Si se produce una compresión, el 
trabajo realizado por 
el entorno es:
$$W=\abs{\vec F} \cdot \abs{\vec{\Delta x}}\cdot \mathrm{cos} \, 0 $$
puesto que $\abs{\vec F}$ y $\abs{\vec{\Delta x}}$ tienen la misma dirección 
y sentido y por tanto $W>0$. Sabiendo que la presión es 
$$P=\frac F A$$
donde $A$ es el área del émbolo podemos escribir:
$$W=P\cdot A \cdot \abs{\vec{\Delta x}}$$
y como el  cambio de volumen es:
$$\Delta V=A\cdot \Delta x$$
tenemos finalmente que:
$$W=-P\cdot \Delta V$$
donde hemos puesto el signo menos para que $W>0$.


\begin{figure}[ht]
 \centering  
\begin{tikzpicture}[scale=0.5]
\draw [](0,6) coordinate (T) circle (2.4 and 0.8); %tapa superior

\draw [](0,4.8) coordinate (T) circle (2.4 and 0.8); % pistón superior
\draw [](0,4.5) coordinate (T) circle (2.4 and 0.8); % pistón superior

\draw[] (0,4.6)--(0,8); % manubrio del pistón

\draw[->] (6,2.6)--(2.5,2.6) node [below right]{cilindro}; % cilindro

\draw[->] (6,4.6)--(2.5,4.6) node [below right]{pistón}; % piston

\draw[] (-2.4,0)--(-2.4,6); % lado izquierdo
\draw[] (2.4,0)--(2.4,6); % lado derecho
% tapa inferior
\draw [](0,0) coordinate (T) circle (2.4 and 0.8);
\end{tikzpicture}
\caption{Cilindro y pistón}
\label{cilindro1}
\end{figure}



\begin{figure}[ht]
 \centering  
\begin{tikzpicture}[scale=0.7]

\draw [](0,6) coordinate (T) circle (2.4 and 0.8); %tapa superior

\draw [](0,4.8) coordinate (T) circle (2.4 and 0.8); % pistón superior
\draw [](0,4.5) coordinate (T) circle (2.4 and 0.8); % pistón superior

\draw[->] (0,4.6)--(0,2.5)node[right]{$\textbf F$}; % fuerza

\draw[->] (3,4.6)--(3,4)node[right]{$\Delta \textbf x$}; % desplazamiento

\draw[dashed] (2.4,4.6)--(7,4.6); % cota superior
\draw[dashed] (2.4,2.1)--(7,2.1); % cota inferior

 % pistón a trazos (inferior)
\draw [dashed](0,2.3) coordinate (T) circle (2.4 and 0.8);
 % pistón a trazos (inferior)
\draw [dashed](0,2) coordinate (T) circle (2.4 and 0.8);

% cota vertical
\draw[<->, dashed] (6,2.1)--(6,4.6) node at (6.6,3.4){$\Delta x$};

% tapa inferior
\draw [](0,0) coordinate (T) circle (2.4 and 0.8);

\draw[] (-2.4,0)--(-2.4,6); % lado izquierdo
\draw[] (2.4,0)--(2.4,6); % lado derecho
\end{tikzpicture}

 \caption{Trabajo de compresión en el cilindro}
\label{cilindro2}
\end{figure}



En una expansión la fuerza realizada por el entorno sigue estando dirigida 
hacia abajo, pero el vector desplazamiento va hacia arriba y por tanto el 
ángulo que forman vale $180^\circ$ y $W<0$. En este caso la fuerza que realiza 
el 
sistema que se encuentra dentro del cilindro es hacia arriba y es la 
responsable de la expansión (consideramos un proceso ideal en que los cambios 
ocurren en infinitos pasos y por eso consideramos que la fuerza que realiza el 
entorno y la que realiza el sistema son iguales pero realmente tiene que haber 
una pequeña descompensación a favor de la fuerza del sistema para que se 
produzca la expansión)

Con esta definición coinciden los criterios de signos del trabajo y del calor y 
así si el sistema absorbe calor ($\mathrm{Q}>0$) y si el entorno realiza 
trabajo sobre el sistema ($\mathrm{W}>0$), en ambos casos se incrementa la 
energía interna.


\subsection{Procesos a volumen constante}
En este caso la primera ley nos queda:
$$\Delta U_V=Q_V-P\Delta V$$
$$\Delta U_V=Q_V$$
puesto que $\Delta V=0$. En el laboratorio trabajamos a volumen constante si 
usamos recipientes cerrados 
y en ese  caso el calor intercambiado nos indica el cambio de la energía 
interna.

\subsection{Procesos a presión constante}
Habitualmente, en el laboratorio, no 
realizamos las 
reacciones en recipientes cerrados sino en abiertos y en ese caso la presión es 
constante  e igual a la atmosférica y la primera ley nos queda:
 \begin{equation}\label{primera_ley}\Delta U_P=Q_P-P\Delta V\end{equation}
 Definimos una magnitud llamada entalpía ($H$):
 $$H=U+PV$$ y su cambio a presión constante:
 $$\Delta H_P= \Delta U_P + \Delta (P V)$$
 $$\Delta H_P= \Delta U_P +  P_2 V_2-P_1V_1$$
 como la presión es constante, $P_1=P_2=P$:
 $$\Delta H_P= \Delta U_P +  P V_2-PV_1$$
 \begin{equation}\label{ent}\Delta H_P= \Delta U_P  + P \Delta 
V\end{equation}
 La ecuación \ref{primera_ley} podemos escribirla:
 $$Q_P=\Delta U_P + P \Delta V$$
 y comparando con la ecuación \ref{ent} escribimos: $$\Delta H_P = 
Q_P$$ que significa que el calor intercambiado a presión constante 
modifica la función de estado entalpía. 

En el laboratorio podemos medir fácilmente el calor intercambiado a 
presión constante y por eso siempre hablaremos de entalpía en vez de 
energía interna. Si queremos saber la energía interna solo tenemos que hacer:
 $$ \Delta U_P = \Delta H_P  - P \Delta V$$
 y vemos que la diferencia entre una y otra solo es debida a que en la 
reacción se modifique el volumen, por ejemplo que se desprendan gases, en cuyo 
caso la energía interna será menor que la entalpía porque se gasta energía al 
producirse una expansión.

\subsection{Aplicaciones en las reacciones químicas}
Vamos a aplicar estos conceptos a las reacciones químicas. Si la reacción 
desprende calor se llama exotérmica y si absorbe calor endotérmica. Por 
ejemplo, la 
formación de amoniaco es exotérmica:
\begin{align*}
\mathrm{N_2 (g) + 3H_2 (g)} & \to \mathrm{2 NH_3 (g)}  & & \Delta
H=-92,2 \ \mathrm{kJ}
\end{align*}
Si ocurre al revés se cambia el signo de la entalpía:
\begin{align*}
\mathrm{2 NH_3 (g)} & \to \mathrm{N_2 (g) + 3H_2 (g)}  & & \Delta
H=+92,2 \ \mathrm{kJ}
\end{align*}
Si se duplican los coeficientes se duplica la entalpía:
\begin{align*}
\mathrm{2N_2 (g) + 6H_2 (g)} & \to \mathrm{4 NH_3 (g)}  & & \Delta
H=-184,4 \ \mathrm{kJ}
\end{align*}

\subsection{Ley de Hess}
La entalpía de una reacción depende solo de los 
estados inicial y 
final y su valor no depende de que transcurra en una o varias etapas.

\subsubsection*{Ejercicios}
\begin{enumerate}[resume]
\item El dióxido de dihidrógeno, conocido como agua oxigenada, se 
descompone fácilmente, transformándose en agua y oxígeno. Calcula la entalpía 
del proceso en el que se forma agua oxigenada a partir del hidrógeno y del 
oxígeno, utilizando los siguientes datos:
\begin{align*}
\mathrm{2H_2 (g) + O_2 (g)} & \to \mathrm{2H_2O (g)}  & & \Delta
H=-571 \ \mathrm{kJ}\\
\mathrm{H_2O_2 (l)} & \to \mathrm{H_2O (l) + \frac 1 2 O_2}  & & \Delta
H=-98 \ \mathrm{kJ}
\end{align*}

Solución. Nos preguntan por el cambio de entalpía que ocurre en esta reacción:
\begin{align*}
\mathrm{H_2 (g) + O_2 (g)} & \to \mathrm{2H_2O_2 (g)}  \hspace{1cm} \Delta H
\end{align*}
Para resolverlo escribimos la primera ecuación dividida entre 2 y la segunda al 
revés, y las sumamos:
\begin{align*}
\mathrm{H_2 (g) + \frac 1 2 O_2 (g)} & \to \mathrm{H_2O (g)}  & & \Delta
H=\frac{-571}{2} \ \mathrm{kJ}\\
\mathrm{H_2O (l) + \frac 1 2 O_2} &   \to \mathrm{H_2O_2 (l)} & & \Delta
H=+98 \ \mathrm{kJ}
\end{align*}
obteniendo:
\begin{align*}
\mathrm{H_2 (g) + O_2 (g)} & \to \mathrm{2H_2O_2 (g)}  \hspace{1cm} \Delta 
H=-187,5\ \mathrm{kJ}
\end{align*}
Por tanto el cambio de entalpía vale -187,5 kJ.
\end{enumerate}

\subsection{Entalpía de formación}
Variación de entalpía que acompaña a la 
formación de 1 
mol de sustancia en estado estándar a partir de sus elementos constituyentes, 
también en sus estados estándar. Para los elementos puros en su estado más 
estable $\Delta H_{\mathrm{f}}^{\circ}=0$. Por ejemplo para O$_2$ $\Delta 
H_{\mathrm{f}}^\circ=0$ y para
O$_3$ $\Delta H_{\mathrm{f}}^\circ=142,7$ kJ/mol.

El estado estándar se indica con el símbolo de grado como superíndice y 
cumple:
\begin{itemize}
 \item $P=1 $ atm
 \item $T=298$ K
 \item 1 molar si es disolución
\end{itemize}

A partir de las entalpías de formación podemos calcular el 
cambio de la entalpía de la reacción:
$$\Delta H_{\mathrm{reacci\acute{o}n}}^\circ= \sum \left[ \Delta 
H_{\mathrm{formaci\acute{o}n}}^\circ \mathrm{\left(productos\right)} - \Delta 
H_{\mathrm{formaci\acute{o}n}}^\circ 
\mathrm{\left(reactivos\right)} \right]$$
y el cuadro~\ref{entalpia} contiene entalpías de formación para diferentes 
sustancias.
\begin{table}[htbp]
\begin{center}
\begin{tabular}{|l|c|}
\hline
sustancia & $ \Delta H_{\mathrm{f}}^\circ(\mathrm{\frac{kJ} {mol}}$) \\
\hline 
etano &  $-84,7$\\ \hline
agua (g) & $-241,8$ \\ \hline
agua (l) & $-285,6$ \\ \hline
CO$_2$ (g) & $-393,5$ \\ \hline

amoniaco (g) & $-46,2$ \\ \hline

\end{tabular}
\caption{Entalpías de formación}
\label{entalpia}
\end{center}
\end{table}

\subsubsection*{Ejercicios}
\begin{enumerate}[resume]
\item Calcula la entalpía de reacción para la combustión del etano a 
partir de las entalpías de formación.

Solución. La reacción ajustada es:
\begin{align*}
\mathrm{C_2H_6 (g)  + \frac 7 2 O_2 (g)}  \to \mathrm{2CO_2 (g) + 
3H_2O(g)}  
\hspace{1cm}\Delta H
\end{align*}
y consultando los valores en el cuadro~\ref{entalpia} aplicamos la ecuación:
$$\Delta H_{\mathrm{reacci\acute{o}n}}^\circ= \sum \left[ \Delta 
H_{\mathrm{formaci\acute{o}n}}^\circ \mathrm{\left(productos\right)} - \Delta 
H_{\mathrm{formaci\acute{o}n}}^\circ
\mathrm{\left(reactivos\right)} \right]$$

$$\Delta H_{\mathrm{reacci\acute{o}n}}^\circ=  \left[ 2\cdot \left(-393,5 
\right) 
+3\cdot \left(  -241,8\right) \right] - \left( -84,7+ 0   \right)$$

$$\Delta H_{\mathrm{reacci\acute{o}n}}^\circ= -1511,8 + 84,7=-1427,1 \, 
\mathrm{kJ}$$


\end{enumerate}
\subsection{Entalpía o energía de enlace}
Entalpía necesaria para romper los  enlaces de un mol de sustancias gaseosas. A 
partir de las energías de enlace podemos calcular el cambio de la entalpía de 
la reacción:
$$\Delta H_\mathrm{r}^\circ= \sum \mathrm{ \left( energ\acute{\dotlessi}a\ de\ 
enlaces\ 
rotos\ 
-\ energ\acute{\dotlessi}a\ de\ enlaces\ formados \right)}$$
y el cuadro~\ref{enlace} contiene entalpías para diferentes tipos de enlace.


\begin{table}[htbp]
\begin{center}
\begin{tabular}{|l|c|}
\hline
sustancia & $ \Delta H_{\mathrm{e}}^\circ(\mathrm{\frac{kJ} {mol}}$) \\
\hline 
H--H & $436$ \\ \hline
C--H &  $414$\\ \hline
C--C & $347$ \\ \hline
H--O & $464$ \\ \hline
O--O & $142$ \\ \hline
O=O & $498$ \\ \hline
C--O & $360$ \\ \hline
C=O & $736$ \\ \hline
\end{tabular}
\caption{Entalpías de enlace}
\label{enlace}
\end{center}
\end{table}

\subsubsection*{Ejercicios}
\begin{enumerate}[resume]
\item  Calcula, a partir de entalpías de enlace, la entalpía de combustión 
del propano 
gas, donde el agua se forma en fase gaseosa.

Solución. La reacción ajustada es:
\begin{align*}
\mathrm{C_3H_8 (g)  + 5 O_2 (g)}  \to \mathrm{3CO_2 (g) + 
4H_2O(g)}  
\hspace{1cm}\Delta H
\end{align*}
Dibujamos las fórmulas desarrolladas y contamos los enlaces rotos y formados 
para calcular la entalpía según:
$$\Delta H_\mathrm{r}^\circ= \sum \mathrm{ \left( energ\acute{\dotlessi}a\ de\ 
enlaces\ rotos\ -\ energ\acute{\dotlessi}a\ de\ enlaces\ formados \right)}$$ 
$$\Delta H_\mathrm{r}^\circ=  \left( 8\cdot \Delta H_{\mathrm{C-H}} + 2\cdot 
\Delta H_{\mathrm{C-C}}+ 5\cdot \Delta H_{\mathrm{O=O}} \right) -  \left(  
3\cdot 2 \cdot  \Delta H_{\mathrm{C=O}} + 4\cdot 2 \cdot \Delta 
H_{\mathrm{O-H}}\right)$$ 
$$\Delta H_\mathrm{r}^\circ=  \left( 8\cdot 414 + 2\cdot 347 + 5\cdot 498 
\right) -  \left(  3\cdot 2 \cdot  736  + 4\cdot 2 \cdot 464\right)$$ $$\Delta 
H_\mathrm{r}^\circ=  6496  - 8128= -1632\ \mathrm{kJ}$$ 
\end{enumerate}


\section{Segunda ley de la termodinámica}
Un proceso en el cual $\Delta U=0$ permite transformar todo el calor en trabajo 
puesto que:
$$\Delta U= Q + W$$
 $$ 0= Q + W$$
 $$W = -Q$$
 
 Como para un gas ideal la energía interna es función solo de la 
temperatura, $U(T)$, un proceso isotermo para un gas ideal cumple que $\Delta 
U=0$. Un ejemplo 
práctico es un cilindro con vapor de agua que se calienta ($Q>0$) y el 
pistón se 
expande realizando un trabajo ($W<0$), manteniéndose la temperatura constante. 
El rendimiento para este proceso es del 100~\%. Pero este ejemplo no tiene 
utilidad práctica, para ello debe trabajar cíclicamente. Un proceso cíclico 
también cumple que $\Delta 
U=0$ y según la primera ley se podría conseguir el 100~\% de rendimiento pero 
la experiencia nos muestra que es imposible (volver a comprimir el pistón y 
enfriar el vapor supone gasto de energía y es la causa de esa imposibilidad). 
Estos hechos conducen a la segunda ley: \begin{quoting}es imposible construir 
una 
máquina 
térmica que trabaje cíclicamente y transforme íntegramente el calor en 
trabajo\end{quoting}. Otro enunciado dice: \begin{quoting}de forma espontánea 
el calor siempre fluye 
del foco caliente al frío\end{quoting}. La figura~\ref{maquinatermica} muestra 
un esquema de una 
máquina térmica (un ejemplo de máquina térmica es el motor de 
combustión de un coche). En ella se cumple:
$$Q_\mathrm{C}>0$$
$$W<0$$
$$Q_\mathrm{F}<0$$
$$\Delta U =Q_\mathrm{C} + Q_\mathrm{F} + W$$
$$0 =Q_\mathrm{C} + Q_\mathrm{F} + W$$
$$Q_\mathrm{C} = -Q_\mathrm{F} - W$$
por tanto no podemos transformar todo el calor extraído del foco caliente en 
trabajo. Un simil hidráulico sería un molino de agua. El agua cae por 
diferencia de altura y mueve las palas. En la máquina térmica el flujo de calor 
se debe a la diferencia de temperatura y el calor produce trabajo al expandirse 
el pistón. En el molino tampoco podemos obtener transformar toda la energía 
cinética del agua en trabajo debido a los rozamientos.

\begin{figure}[ht]
\centering
\begin{tikzpicture}
\pgfplotsset{width=7cm,compat=1.16}

\draw[] (0,0) rectangle (3.5,1.2) node[below left] {Máquina térmica};
\node at (4.2,0.65){$\Longrightarrow$};
\node at (4.2,0.25){$W$};
\node at (1.9,2.3) {Foco caliente};
\node at (1.8,1.6){$\Downarrow$};
\node at (2.3,1.6){$Q_\mathrm C$};
\node at (1.8,-0.4){$\Downarrow$};
\node at (2.3,-0.4){$Q_\mathrm F$};
\node at (1.9,-1){Foco frío};
\end{tikzpicture}
\caption{Máquina térmica}
\label{maquinatermica}
\end{figure}


Para un proceso cualquiera comprobamos experimentalmente que el cociente $\frac 
{\Delta Q} {T}$  solo depende
del estado  inicial y final, por tanto  dicha magnitud es una función 
de estado. Por eso se define una nueva magnitud, llamada entropía ($S$), cuya 
variación cumple:
$$ \Delta S=\frac {\Delta Q} {T}$$


Microscópicamente hablando la entropía nos indica el 
número de microestados posibles del sistema de tal manera que a más 
microestados más entropía. Se suele indicar que a más entropía más desorden 
pero es solo una metáfora para entender el concepto. Variación de la entropía 
para algunos procesos:
\begin{itemize}
 \item $S_{\mathrm{gas}} 
>S_{\mathrm{l\acute{\dotlessi}quido}}>S_{\mathrm{s\acute{o}lido} } $
\item Al mezclar dos gases A y B: $S_{\mathrm{A+B}}>  S_\mathrm{A} 
+S_\mathrm{B}$
\item Si $T_2 > T_1$: $S(T_2)> S(T_1)$
\item $ 
S_{\mathrm{disoluci\acute{o}n}} > S_{\mathrm{soluto}} + 
S_{\mathrm{disolvente}} $
\item $S_\mathrm{alumnado\, en\, el\, patio} > S_\mathrm{alumnado\, en\, clase}$
\end{itemize}

Otro enunciado de la segunda ley es que en todo proceso espontáneo la 
entropía aumenta ($\Delta S > 
0$).
Por otro lado un proceso en el que su energía disminuye también es espontáneo y 
para unificar ambos criterios definimos la energía libre de Gibbs:
$$G=H-TS$$
Para un proceso a $P$ y $T$ constante:
$$\Delta G = \Delta H - T \Delta S$$
y si:
\begin{itemize}
 \item $\Delta G<0 \Rightarrow$ espontáneo
 \item $\Delta G=0 \Rightarrow$ equilibrio
 \item $\Delta G>0 \Rightarrow$ no espontáneo
\end{itemize}

\subsubsection*{Ejercicios}
\begin{enumerate}[resume]
\item En la solidificación del agua $\Delta H=-6$ kJ y $\Delta 
S=-22$ $ \mathrm{J/K}$. Justifica por qué el agua no congela a $25~^\circ$C 
y sí lo hace a $-1~^\circ$C.

Solución. Calculamos la energía libre de Gibbs a ambas temperaturas:
$$\Delta G = \Delta H - T \Delta S=\Delta G = -6000 - 298\cdot(-22)= 
-6000+6556=556 \ \mathrm{J}$$
$$\Delta G = \Delta H - T \Delta S=\Delta G = -6000 - 272\cdot(-22)= 
-6000+5984=-16 \ \mathrm{J}$$
y solo el segundo caso es espontáneo al ser $\Delta G<0.
$
\item Determina si la formación de amoniaco a partir de nitrógeno e hidrógeno 
es 
espontánea a $25~^\circ$C. ¿A partir de qué temperatura no es espontánea? 
Datos: para 
la obtención de 2 moles de amoniaco, $\Delta H=-92,2$ kJ y $\Delta S=-200$ J/K.

Solución. Calculamos $\Delta G$:
$$\Delta G = \Delta H - T \Delta S=\Delta G = -92200 - 298\cdot(-200)=-92200 + 
59600=-32,6 \ \mathrm{kJ}$$
y como es negativo sí es espontánea.

El equilibro ocurre cuando $\Delta G=0$ por tanto impongamos esa condición a la 
energía libre de Gibbs:
$$\Delta G = \Delta H - T \Delta S$$
$$0 = \Delta H - T \Delta S$$
y despejemos la temperatura:
$$  T \Delta S= \Delta H$$
$$  T = \frac{\Delta H}{\Delta S}=\frac{-92200}{-200}=\frac{922}{2}=461\ 
\mathrm K$$
y para $T>461$ K la reacción no es espontánea porque incrementamos el término 
positivo en la energía libre de Gibbs.
\end{enumerate}

\section{Tercera ley de la termodinámica}
La tercera ley establece que la entropía de un cristal puro y 
perfecto en el cero absoluto de temperatura es cero. Permite, por tanto, tener 
una escala absoluta de entropías y calcular su variación para una reacción 
mediante:
 
$$\Delta S_{\mathrm{reacci\acute{o}n}} = \sum \left(S_{\mathrm{productos}} - 
S_{\mathrm{reactivos}}\right)$$ (el cuadro~\ref{entropia} contiene valores de 
entropía estándar para diferentes sustancias).


\begin{table}[htbp]
\begin{center}
\begin{tabular}{|l|c|}
\hline
sustancia & $S^\circ$ ($\mathrm{\frac{J} {mol \cdot  K}}$) \\
\hline 
C (grafito) & 5,7 \\ \hline
C (diamante) & 2,38 \\ \hline
O$_2$ (g) & 205,1 \\ \hline
CO$_2$ (g) & 213,7 \\ \hline
CO (g) & 197,7 \\ \hline
Hg (l) & 76 \\ \hline
HgO (s) & 70,3 \\ \hline
HCl (g) & 186,9 \\ \hline
NH$_3$ (g) & 192,5 \\ \hline
NH$_4$Cl (s) & 94,6 \\ \hline

\end{tabular}
\caption{Entropías estándar}
\label{entropia}
\end{center}
\end{table}

\subsubsection*{Ejercicios}
\begin{enumerate}[resume]

\item Determina la variación de entropía de las siguientes reacciones:

\textit{a})~C (s) + O$_2$ +  $\to$ CO$_2$ (g)\\
\textit{b})~C (s) + $\frac 1 2 $ O$_2$  $\to$ CO (g)\\
\textit{c})~CO (g) + $\frac 1 2 $ O$_2$  $\to$ CO$_2$ (g)\\
\textit{d})~2 HgO (s) $\to$ 2Hg (l) + O$_2$ (g)\\
\textit{e})~HCl (g) + NH$_3$ (g) $\to$ NH$_4$Cl (s)

Solución. 

\textit{a})~$\Delta S_{\mathrm{reacci\acute{o}n}} = \sum 
\left(S_{\mathrm{productos}} - 
S_{\mathrm{reactivos}}\right)=213,7 - (5,7 + 205,1)=2,9\ \mathrm{J}$

\textit{b})~$\Delta S_{\mathrm{reacci\acute{o}n}} = 197,7 - (5,7 + 
\frac 1 2 \cdot 205,1)=89,45\ \mathrm{J}$

\textit{c})~$\Delta S_{\mathrm{reacci\acute{o}n}} = 213,7 - (197,7  + 
\frac 1 2 \cdot 205,1)= - 86,55\ \mathrm{J}$

\textit{d})~$\Delta S_{\mathrm{reacci\acute{o}n}} = 2\cdot 76+ 205,1 - 
2\cdot 70,3 =216,5\ \mathrm{J}$

\textit{e})~$\Delta S_{\mathrm{reacci\acute{o}n}} = 94,6 - (186,9 + 
192,45)= - 284,75\ \mathrm{J}$
\item Sabiendo que las entalpías molares de combustión, en kJ/mol, del 
propano, del grafito y del hidrógeno, son $-2219$, $-393$ y $-285$
respectivamente, calcula la entalpía del proceso:
$$3 \mathrm{ C(s)\ + 4 H_2\ (g)\ \to\ C_3H_8 (g)}$$

Solución. Nos dicen cuanta energía se desprende al quemar un mol de propano, 
grafito e hidrógeno:
\begin{align*}
\mathrm{C_3H_8 (g) + 5O_2 (g)} & \to \mathrm{3CO_2 (g)+  4H_2O (g)} &\Delta 
H=-2219 \ \mathrm{kJ}\\
\mathrm{C (s) + O_2 (g) }      & \to \mathrm{CO_2 (g)}   &\Delta H=-393 \ 
\mathrm{kJ}\\
\mathrm{H_2 (g) + \frac 1 2 O_2 (g)} & \to \mathrm{H_2O (g)}  
& \Delta H=-285 \ \mathrm{kJ}
\end{align*}
Multiplicamos la primera ecuación por -1, la segunda por 3 y la tercera por 4, 
obtenemos:
\begin{align*}
 \mathrm{3CO_2 (g)+  4H_2O (g)} & \to \mathrm{C_3H_8 (g) + 5O_2 (g)} &\Delta 
H=2219 \ \mathrm{kJ}\\
\mathrm{3C (s) + 3O_2 (g) }      & \to \mathrm{3CO_2 (g)}   &\Delta H=-1179 \ 
\mathrm{kJ}\\
\mathrm{4H_2 (g) +  2 O_2 (g)} & \to \mathrm{4H_2O (g)}  
& \Delta H=-1140 \ \mathrm{kJ}
\end{align*}
y sumándolas obtenemos la reacción pedida. Por la ley de Hess sabemos 
que la entalpía de la reacción pedida es:
$$\Delta H=2219) + (-1179) + (-1140)=-100\ \mathrm{kJ}$$

\item Al quemar 1~g de etanol y 1~g de ácido acético se desprenden 29,7~kJ y 
14,5~kJ, respectivamente. Determina cuál de las dos sustancias tiene mayor 
entalpía de combustión por mol.

Solución. Pasamos a cantidad de sustancia:
$$\Delta H_\mathrm{C_2H_6O}=\mathrm{\frac {29,7\ kJ}{1\, g}  }
\cdot \mathrm{\frac{46\,g}{1\,mol}}=1366,2\,\mathrm{\frac{kJ}{mol}}$$
$$\Delta H_\mathrm{C_2H_4O_2}=\mathrm{\frac {14,5\ kJ}{1\, g}  }
\cdot \mathrm{\frac{60\,g}{1\,mol}}=870\,\mathrm{\frac{kJ}{mol}}$$
por lo que el etanol tiene más energía de combustión por mol.

\item Calcula la entalpía de la reacción del etino con hidrógeno para dar etano 
a partir de las entalpías de combustión del etino y del etano, cuyos valores 
son, respectivamente, $-1301$ y $-1560$ kJ/mol, y de la entalpía de formación 
del agua líquida cuyo valor es $-285$~kJ/mol.

Solución. Nos preguntan por la entalpía de esta reacción:
\begin{align*}
\mathrm{C_2H_2 (g) + 2 H_2 (g)} & \to \mathrm{C_2H_6 (g)} 
&\Delta H
\end{align*}
y nos dan los siguientes datos:
\begin{align*}
\mathrm{C_2H_2 (g) + \frac 5 2 O_2 (g)} & \to \mathrm{2CO_2 (g)+  H_2O (g)} 
&\Delta H=-1301 \ \mathrm{kJ}\\
\mathrm{C_2H_6 (g) + \frac 7 2 O_2 (g)} & \to \mathrm{2CO_2 (g)+  3H_2O (g)} 
&\Delta H=-1560 \ \mathrm{kJ}\\
\mathrm{H_2 (g) + \frac 1 2 O_2 (g)} & \to \mathrm{H_2O (g)}  
& \Delta H=-285 \ \mathrm{kJ}
\end{align*}
Multiplicamos la primera ecuación por 1, la segunda por -1 y la tercera por 2 
y sumándolas todas 
obtenemos la reacción que nos piden y aplicando la ley de Hess:
$$\Delta H=(1)\cdot (-1301) + (-1)\cdot (-1560) + 2\cdot (-285)=-311\ 
\mathrm{kJ}$$

\item Calcula la entalpía de formación del etino, a partir de las entalpías de 
formación de agua líquida, $-285$~kJ/ml, dióxido de carbono, $-393$~kJ/mol y 
del calor de combustión del etino, $-1300$~kJ/mol.

Solución. Nos preguntan por esta reacción:
\begin{align*}
\mathrm{2C (s) +  H_2 (g)} & \to \mathrm{C_2H_2 (g)} 
&\Delta H
\end{align*}
y tenemos estos datos:
\begin{align*}
\mathrm{H_2 (g) + \frac 1 2 O_2 (g)} & \to \mathrm{H_2O (g)}  
& \Delta H=-285 \ \mathrm{kJ}\\
\mathrm{C (s) + O_2 (g) }      & \to \mathrm{CO_2 (g)}   &\Delta H=-393 \ 
\mathrm{kJ}\\
\mathrm{C_2H_2 (g) + \frac 5 2 O_2 (g)} & \to \mathrm{2CO_2 (g)+  H_2O (g)} 
&\Delta H=-1300 \ \mathrm{kJ}
\end{align*}
Multiplicamos la segunda por 2 y la tercera por -1:
\begin{align*}
\mathrm{H_2 (g) + \frac 1 2 O_2 (g)} & \to \mathrm{H_2O (g)}  
& \Delta H=-285 \ \mathrm{kJ}\\
\mathrm{2C (s) + 2O_2 (g) }      & \to \mathrm{2CO_2 (g)}   &\Delta H=-786 \ 
\mathrm{kJ}\\
 \mathrm{2CO_2 (g)+  H_2O (g)}& \to \mathrm{C_2H_2 (g) + \frac 5 2 O_2 (g)} 
&\Delta H=+1300 \ \mathrm{kJ}
\end{align*}
y al sumar obtenemos la reacción pedida y la variación de entalpía es la suma 
de cada variación individual, debido a la ley de Hess:
$$\Delta H=-285 -786 +1300=229\ 
\mathrm{kJ}$$
\end{enumerate}

\end{document}
