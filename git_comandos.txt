https://git-scm.com/book/es/v1

apt-get install git-core

git init
git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
git add file
git commit -a -m "comentario"  # confirmar los cambios hechos sin pasar por 
staged (área de preparación).
git commit --amend  añade comentarios al commit anterior.
git status

.gitignore para poner archivos que no queremos que liste como untracked

para añadir el remoto si no se ha creado el local clonando
git remote add origin https://llantones@bitbucket.org/llantones/1_bach.git


clonar del remoto al local
git clone https://llantones@bitbucket.org/llantones/1_bach.git


Si trabajo con varios repositorios locales y uno remoto central, antes de
trabajar hacer siempre git pull para bajar los cambios.

subir al remoto
git push


descargar cambios del remoto:
git pull  # sobrescribe en el local (es un fetch y un merge)
git fetch # no sobrescribe en el local, lo deja en la rama espejo
# del servidor llamada origin/master solo se ve con git branch -a
# así podemos ver que hemos bajado antes de fusionar.
git merge # fusiona con el local

git diff # diferencias entre archivo en el directorio y en git local
git diff commit commit  # muestra diferencias entre commits

histórico de confirmaciones:
git log  # muestra cambios enviados a git por commit. Solo muestra el comentario
git log -p # muestra los cambios del commit. Las líneas implicadas las dice
entre @@, indicando el nº de la línea y el rango, por ejemplo, @@  -6,5 +6,6 @@
significa que se quita de la línea 6 a la 6+5 y se añade de la 6 a la 6+6.
git log --pretty=oneline
git log -p --word-diff # diferencias por palabras. En versión 1.7 no me funcionó
git log -U1 --word-diff # igual pero sin informacion de contexto
git show nºcommit muestra solo los cambios de ese commit
git show HEAD^^  == git show HEAD~2
recuperar versiones:
git checkout -- fichero # deja el fichero como estaba si lo había modificado
pero no había hecho commit (o poner '*')
git checkout nºcommit # pone el repositorio como en ese commit. Si pretendemos
# modificar otra vez a partir de ese commit mejor crear una rama:
git checkout nºcommit -b nuevarama
git revert numero_de_commit  # deshace ese commit y deja todos los demás commit
# que hayamos hecho. Y el deshacer queda en la historia.
git reset --hard numero_de_commit  # pone el directorio como estaba en ese commit. Ojo, 
# no hacer ahora git push porque machacamos los cambios posteriores. Si hacemos
# git pull volvemos a la última versión. Operación delicada si se comparte el repositorio.
# mejor usar revert
git show numero_de_commit:fichero > fichero_original recupera la versión de 
# fichero del commit en fichero_original
git checkout -p nºcommit #fusiona el nºcommit con el HEAD preguntando qué hacer 
con cada diferencia, permitiendo incluso editar el cambio. Relacionado con esta
opción está la -i y la -e en el comando git diff.

git remote -v  # muestra repositorio remoto y su URL


ramas:
git branch rama_1  # crea la rama rama_1
git checkout rama_1  # cambia a la rama rama_1
git checkout -b rama_1 # crea y cambia en un solo paso
git checkout nºcommit -b nombrerama # crea rama a partir del commit
git push origin paginas_separadas  # subir la rama al remoto:
git branch -d rama_1  # borra la rama rama_1
git push origin --delete rama_1 # borrar rama remota

git checkout master  # volver a master
git merge rama_1 fusiona la rama_1 con la que estés actualmente

Tenemos estas ramas

master      1-2-3-4-5
              |
prueba        6-7-8-9-10

El commit 6 no lo quiero pero sí el 7, 8, 9 y 10. 
Y quiero fusionarlos con la rama de 1, 2, 3, 4, 5.
Para ello usamos git cherry-pick nº de commit
A partir de la versión 1.7.2 se puede indicar un rango de commits
si no uno a uno.

Practica de git con bitbucket. 
-que configuren su nombre y apellidos en la cuenta para que aparezcan en los 
commits en vez del nombre de usuario.



bitbucket
uso la cuenta llantones luis.munoz.edu@juntadeandalucia.es
Le añado un equipo (tic_bach) y un proyecto (tic_bach) y dentro de él le añado 
dos repositorios públicos (tic_latex y tic_html). Añado alumnos al equipo.
