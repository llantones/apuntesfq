Apuntes de Física y Química en LaTex para modificar y adaptar con licencia de documentación libre.

- problemasfisica1.tex contiene en su inmensa mayoría problemas extraídos de pruebas de selectividad.
- problemas_fisica_resueltos.tex contiene los problemas del fichero anterior resueltos.
- formu.tex son apuntes realizados por mi. Contienen fórmulas que no existen en la naturaleza pensando solo en practicar con diferentes combinaciones de elementos.
- problemas_fq.tex contiene en su inmensa mayoría problemas del libro Física y Química 1º Bachillerato 1ª edición de la editorial Oxford 
  de los autores Mario Ballestero Jadraque y Jorge Barrio Gómez de Agüero (supongo que estaré infringiendo la licencia del libro, pueden ponerme los grilletes...) y  problemas_fq_resueltos.tex contiene las soluciones a dichos enunciados realizadas por mi. 
- problemas_fq_resueltos.tex contiene los problemas del fichero anterior resueltos.
- termo.tex contiene apuntes y resoluciones de ejercicios propios y enunciados de ejercicios extraídos la mayoría del libro Física y Química 1º Bachillerato 1ª edición de 
  la editorial Oxford de los autores Mario Ballestero Jadraque y Jorge Barrio Gómez de Agüero (supongo que estaré infringiendo la licencia del libro, 
  pueden ponerme los grilletes...).


Copyleft Luis Muñoz Fuente

luis.munoz.edu arroba juntadeandalucia.es

Licencia Creative Commons cc-by-sa 3.0 ES

Usted es libre de:

    Compartir: copiar y redistribuir (incluyendo el uso comercial) el material en cualquier medio o formato.
    Adaptar: remezclar, transformar y crear a partir del material para cualquier finalidad, incluso comercial.

Bajo las condiciones siguientes:

    Reconocimiento: debe reconocer adecuadamente la autoría e indicar si se han realizado cambios.
    Compartir igual: si remezcla, transforma o crea a partir del material, deberá difundir sus 
    contribuciones bajo la misma licencia que el original facilitando el código fuente del material.

Licencia completa: https://creativecommons.org/licenses/by-sa/3.0/es/legalcode.es
